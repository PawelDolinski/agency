$(document).ready(function () {
    
    setTimeout(function(){
        $('.header__text').fadeIn(200);
    }, 2000);


    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > 50) {
            $('#nav-menu').addClass('fixed');
        } else {
            $('#nav-menu').removeClass('fixed');
        }

    });

    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > 500) {
            $('.icon-wrapper').addClass('animation');
        }

    });
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > 1000) {
            $('.portfolio-left-column').addClass('portfolio-animated-left');
        }

    });
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > 1000) {
            $('.portfolio-right-column').addClass('portfolio-animated-right');
        }

    });
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > 2000) {
            $('.contact-us form').addClass('contact-us-animation');
            $('#contact-us button').addClass('contact-us-animation-button');
            $('#contact-us h2').addClass('contact-us-animation');
        }

    });


    /*    scroll page  */

    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    });

    /*drop menu */

    $('#toggle-button').click(function () {
        $('#toggle-menu-wrapper').toggleClass('active');
        $('#toggle-nav-menu li').toggleClass('open');
    });

    $('#toggle-nav-menu li').click(function () {
        $('#toggle-menu-wrapper').toggleClass('active');
        $('#toggle-nav-menu li').toggleClass('open');

    });

});
